export class Redis {
  data: { [key: string]: string } = {};

  async set(key: string, value: string) {
    this.data[key] = value;
    return this.data[key];
  }

  async get(key: string) {
    return this.data[key];
  }
}
