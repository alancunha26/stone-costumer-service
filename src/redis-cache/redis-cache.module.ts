import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Redis from 'ioredis';

@Global()
@Module({
  providers: [
    {
      provide: Redis,
      inject: [ConfigService],
      useFactory: (configService: ConfigService) =>
        new Redis({
          port: configService.get('REDIS_PORT') || 6379,
          host: configService.get('REDIS_HOST') || 'redis',
        }),
    },
  ],
  exports: [Redis],
})
export class RedisCacheModule {}
