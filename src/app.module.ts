import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { KeycloakConnectModule, AuthGuard } from 'nest-keycloak-connect';
import { CostumersModule } from './costumers/costumers.module';
import { RedisCacheModule } from './redis-cache/redis-cache.module';

@Module({
  imports: [
    KeycloakConnectModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        authServerUrl: configService.get('KEYCLOAK_URL'),
        secret: configService.get('KEYCLOAK_SECRET'),
        clientId: 'customers',
        realm: 'careers',
      }),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    CostumersModule,
    RedisCacheModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
