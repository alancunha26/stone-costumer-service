import IORedis from 'ioredis';
import * as supertest from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { CostumersController } from './costumers.controller';
import { CostumersService } from './costumers.service';
import { Redis } from '../redis-cache/redis-cache.mock';

describe('CostumersController', () => {
  let service: CostumersService;
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CostumersController],
      providers: [
        CostumersService,
        {
          provide: IORedis,
          useClass: Redis,
        },
      ],
    }).compile();

    service = module.get<CostumersService>(CostumersService);
    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  it('POST should status code 201 and create costumer', async () => {
    const mockCostumer = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const response = await supertest(app.getHttpServer())
      .post('/costumers')
      .send(mockCostumer)
      .expect(201);

    expect(response.body).toHaveProperty('id');
    expect(response.body).toHaveProperty('name');
    expect(response.body.name).toBe(mockCostumer.name);
    expect(response.body).toHaveProperty('document');
    expect(response.body.document).toBe(mockCostumer.document);
  });

  it('POST should status code 400 when input invalid body', async () => {
    const mockCostumer = {
      name: 26042000,
      // document: 11080781927,
    };

    await supertest(app.getHttpServer())
      .post('/costumers')
      .send(mockCostumer)
      .expect(400);
  });

  it('PATCH/:id should status code 200 and create costumer', async () => {
    const mockCostumer = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const createdCostumer = await service.create(mockCostumer);
    const updatedProperties = { name: 'Alan Alegre da Cunha' };

    const response = await supertest(app.getHttpServer())
      .patch(`/costumers/${createdCostumer.id}`)
      .send(updatedProperties)
      .expect(200);

    expect(response.body).toHaveProperty('id');
    expect(response.body).toHaveProperty('name');
    expect(response.body.name).toBe(updatedProperties.name);
    expect(response.body).toHaveProperty('document');
    expect(response.body.document).toBe(mockCostumer.document);
  });

  it('PATCH/:id should status code 404 when try to update non existent costumer', async () => {
    await supertest(app.getHttpServer())
      .patch('/costumers/non_existent_id')
      .send({ name: 'Alan Alegre da Cunha' })
      .expect(404);
  });

  it('PATCH/:id should status code 409 when try to update a costumer id that already exists', async () => {
    expect(service).toBeDefined();

    const mockCostumerA = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const mockCostumerB = {
      name: 'Samantha da Silva',
      document: 27650539016,
    };

    const costumerA = await service.create(mockCostumerA);
    const costumerB = await service.create(mockCostumerB);

    await supertest(app.getHttpServer())
      .patch(`/costumers/${costumerA.id}`)
      .send({ id: costumerB.id })
      .expect(409);
  });

  it('PATCH/:id should status code 400 when input invalid body', async () => {
    const costumer = await service.create({
      name: 'Alan Cunha',
      document: 11080781927,
    });

    await supertest(app.getHttpServer())
      .patch(`/costumers/${costumer.id}`)
      .send({ id: 1 })
      .expect(400);
  });

  it('GET/:id should status code 200 and return a costumer', async () => {
    const createdCostumer = await service.create({
      name: 'Alan Cunha',
      document: 11080781927,
    });

    const response = await supertest(app.getHttpServer())
      .get(`/costumers/${createdCostumer.id}`)
      .expect(200);

    expect(response.body).toHaveProperty('id');
    expect(response.body).toHaveProperty('name');
    expect(response.body.name).toBe(createdCostumer.name);
    expect(response.body).toHaveProperty('document');
    expect(response.body.document).toBe(createdCostumer.document);
  });

  it('GET/:id should status code 404 when try to get a user that not exists', async () => {
    await supertest(app.getHttpServer())
      .get('/costumers/non_existent_id')
      .expect(404);
  });
});
