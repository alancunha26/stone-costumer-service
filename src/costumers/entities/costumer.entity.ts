export class CostumerEntity {
  id: string;
  name: string;
  document: string;

  constructor(partial: Partial<CostumerEntity>) {
    Object.assign(this, partial);
  }
}
