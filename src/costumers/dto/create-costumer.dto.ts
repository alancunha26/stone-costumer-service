import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateCostumerDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNumber()
  @IsNotEmpty()
  document: number;
}
