import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateCostumerDto {
  @IsString()
  @IsOptional()
  id?: string;

  @IsString()
  @IsOptional()
  name?: string;

  @IsNumber()
  @IsOptional()
  document?: number;
}
