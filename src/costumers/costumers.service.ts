import Redis from 'ioredis';
import { v4 } from 'uuid';
import {
  Injectable,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import { CreateCostumerDto } from './dto/create-costumer.dto';
import { UpdateCostumerDto } from './dto/update-costumer.dto';
import { CostumerEntity } from './entities/costumer.entity';

@Injectable()
export class CostumersService {
  constructor(private readonly redis: Redis) {}

  async create(createCostumerDto: CreateCostumerDto) {
    const id = v4();
    const key = `costumer:${id}`;
    await this.redis.set(key, JSON.stringify({ ...createCostumerDto, id }));
    const costumer = await this.redis.get(key);
    return new CostumerEntity(JSON.parse(costumer));
  }

  async update(id: string, updateCostumerDto: UpdateCostumerDto) {
    let key = `costumer:${id}`;
    const costumer = await this.redis.get(key);

    if (!costumer) {
      throw new NotFoundException(`Costumer with id ${id} not found`);
    }

    const costumerEntity = new CostumerEntity({
      ...JSON.parse(costumer),
      ...updateCostumerDto,
    });

    if (updateCostumerDto.id) {
      const conflictKey = `costumer:${updateCostumerDto.id}`;
      if (await this.redis.get(conflictKey)) {
        throw new ConflictException(
          `Costumer with id ${updateCostumerDto.id} already exists`,
        );
      }

      this.redis.del(key);
      key = conflictKey;
    }

    await this.redis.set(key, JSON.stringify(costumerEntity));
    return costumerEntity;
  }

  async findOne(id: string) {
    const key = `costumer:${id}`;
    const costumer = await this.redis.get(key);

    if (!costumer) {
      throw new NotFoundException(`Costumer with id ${id} not found`);
    }

    return new CostumerEntity(JSON.parse(costumer));
  }
}
