import IORedis from 'ioredis';
import { Test, TestingModule } from '@nestjs/testing';
import { CostumersService } from './costumers.service';
import { CostumerEntity } from './entities/costumer.entity';
import { Redis } from '../redis-cache/redis-cache.mock';
import { ConflictException, NotFoundException } from '@nestjs/common';

describe('CostumersService', () => {
  let service: CostumersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CostumersService,
        {
          provide: IORedis,
          useClass: Redis,
        },
      ],
    }).compile();

    service = module.get<CostumersService>(CostumersService);
  });

  it('should create a costumer', async () => {
    expect(service).toBeDefined();

    const mockCostumer = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const costumer = await service.create(mockCostumer);
    expect(costumer).toBeDefined();
    expect(costumer).toBeInstanceOf(CostumerEntity);
    expect(costumer.name).toBe(mockCostumer.name);
    expect(costumer.document).toBe(mockCostumer.document);
  });

  it('should update the costumer name', async () => {
    expect(service).toBeDefined();

    const mockCostumer = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const name = 'Alan Alegre da Cunha';
    const createdCostumer = await service.create(mockCostumer);
    const costumer = await service.update(createdCostumer.id, { name });

    expect(costumer).toBeDefined();
    expect(costumer).toBeInstanceOf(CostumerEntity);
    expect(costumer.name).toBe(name);
  });

  it('should catch a NotFoundException when trying to update non existent costumer', () => {
    expect(service).toBeDefined();

    const name = 'Alan Alegre da Cunha';
    expect(service.update('non_existent_id', { name })).rejects.toThrow(
      NotFoundException,
    );
  });

  it('should catch a ConflictException when trying to update an id that already exists', async () => {
    expect(service).toBeDefined();

    const mockCostumerA = {
      name: 'Alan Cunha',
      document: 11080781927,
    };

    const mockCostumerB = {
      name: 'Samantha da Silva',
      document: 27650539016,
    };

    const costumerA = await service.create(mockCostumerA);
    const costumerB = await service.create(mockCostumerB);
    const updatedCostumer = service.update(costumerA.id, { id: costumerB.id });
    expect(updatedCostumer).rejects.toThrow(ConflictException);
  });

  it('should find a costumer', async () => {
    const createdCostumer = await service.create({
      name: 'Alan Cunha',
      document: 11080781927,
    });

    const costumer = await service.findOne(createdCostumer.id);
    expect(costumer).toBeDefined();
    expect(costumer).toBeInstanceOf(CostumerEntity);
  });

  it('should catch a NotFoundException when trying to find a non existent costumer', async () => {
    const costumer = service.findOne('non_existent_id');
    expect(costumer).rejects.toThrow(NotFoundException);
  });
});
