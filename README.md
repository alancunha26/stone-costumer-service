<p align="center">
  <img src="https://1.bp.blogspot.com/-GYQJzo4aQe8/YBNIO5O2hpI/AAAAAAAFcq8/JyQWiE0VhyY3LcQkBe9TrsMWAAiKI1y4ACLcBGAsYHQ/s1844/unnamed%2B%25281%2529.png" width="240" alt="Nest Logo" />
</p>

## Description

The Stone NestJS Alan Cunha's evaluation project.

## Installation

```bash
$ npm install
```

## Setup enviroment

This project was developed using the following dependencies or you can go the next step and run with Docker

```
Node v17.9.0 (LTS)

Redis 6
```

Duplicate your .env.example to a .env file and define the following enviroment variables

`SERVER_PORT` - the port running the application;

`KEYCLOAK_URL` - the keycloak service authentication url;

`KEYCLOAK_SECRET` - the secret key to validade authentication;

`REDIS_HOST` - the connection host that your redis is running;

`REDIS_PORT` - the connection port that your redis is listenning;

## Running with Docker

If you want a fast setup, you can use the docker configuration. Just ensure you have docker installed and run

```bash
$ npm run start:docker
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
